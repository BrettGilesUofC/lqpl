\incsec{Compiler main functions}\label{incsec:compiler calling}

\begin{code}

module Compiler.BaseTypes(
                          CompilerLogs
                         ) where


type CompilerLogs = [String]

\end{code}
