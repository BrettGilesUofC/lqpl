# encoding: utf-8
# classical node painter
class ClassicalDescriptorPainter < AbstractDescriptorPainter
  def my_colour
    Color.green
  end
end
