# encoding: utf-8
# Zero value painter
class ZeroDescriptorPainter < AbstractDescriptorPainter
  def my_colour
    Color.black
  end
end
