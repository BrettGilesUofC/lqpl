# encoding: utf-8
# Class to signal bad input
class InvalidInput < RuntimeError
end
