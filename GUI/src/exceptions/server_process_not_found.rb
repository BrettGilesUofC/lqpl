# encoding: utf-8
# Class to signal server process has not been found
class ServerProcessNotFound < RuntimeError
end
