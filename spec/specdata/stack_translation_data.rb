# encoding: UTF-8
P1 =  '{"memory_map" :[{"p" : 1}]}'
P2 =  '{"memory_map" :[{"r" : 1}]}'
P1WITHREX27 = '{"memory_map" :[{"p" : 1, "rex": 27}]}'
P1ANDR1 = '{"memory_map" :[{"p" : 1}, {"r": 1}]}'
P1ANDR1ANDS1 = '{"memory_map" :[{"p" : 1}, {"r": 1}, {"s": 1}]}'

P1ANDEMPTYANDS1 = '{"memory_map" :[{"p" : 1}, {}, {"s": 1}]}'
P1ANDEMPTY = '{"memory_map" :[{}, {"p" : 1}]}'

EMPTYSTACK = '{"memory_map" :[{}]}'

Q1R2 = '{"memory_map" :[{"@q":1, "@r" :2}]}'

L3STACK = '{"memory_map" :[{"p" : 1},{"p" : 2},{"rex": 27, "p":3}]}'
